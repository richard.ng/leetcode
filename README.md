# Leet code solutions

| Problem | Golang | Python |
|---|:---:|:---:|
| [Fibonacci Number](https://leetcode.com/problems/fibonacci-number/) | ✔️ | ✔️ |
| [Hamming Distance](https://leetcode.com/problems/hamming-distance/) | ✔️ | ❌ |
| [Jewels and Stones](https://leetcode.com/problems/jewels-and-stones/) | ✔️ | ✔️ |
| [Palindrome Number](https://leetcode.com/problems/palindrome-number/) | ✔️ | ✔️ |
| [Sort Array by Parity](https://leetcode.com/problems/sort-array-by-parity/) | ✔️ | ❌ |
| [To Lower Case](https://leetcode.com/problems/to-lower-case/) | ✔️ | ✔️ |
