#  https://leetcode.com/problems/how-many-numbers-are-smaller-than-the-current-number/

class Solution:
  def smallerNumbersThanCurrent(self, nums: List[int]) -> List[int]:
    List = []
    for i, j in enumerate(nums):
      counter = 0
      for k, l in enumerate(nums):
        if j > l:
          counter += 1
      List.append(counter)
    return List
