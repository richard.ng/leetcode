#  https://leetcode.com/problems/first-missing-positive/

class Solution:
    def firstMissingPositive(self, nums: List[int]) -> int:

        positives = []
        sorted = []

        for i in nums:
          if i >= 1:
            positives.append(i)

        for i in positives:
          if i not in sorted:
            sorted.append(i)
        sorted.sort()

        if len(sorted) == 0:
          return(1)

        for i,j in enumerate(sorted):
          if i+1 != j:
            return(i+1)
            break
          if i == len(sorted)-1:
            return(j+1)
