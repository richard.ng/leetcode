#  https://leetcode.com/problems/two-sum/

class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:

        indices = []

        for i, j in enumerate(nums):

            new = nums.copy()
            new.remove(j)

            if target-j in new:
                indices.append(i)

        return(indices)
