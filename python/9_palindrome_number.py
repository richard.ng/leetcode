# https://leetcode.com/problems/palindrome-number/

class Solution:
    def isPalindrome(self, x: int) -> bool:
        if x < 0:
            return False
        if x < 10:
            return True
        y = str(x)
        leng = int(len(y)/2)
        for i in range(leng):
            if y[i] != y[len(y)-1-i]:
                return False
        
        for i in range(leng):
            if y[i] == y[len(y)-1-i]:
                return True