# https://leetcode.com/problems/roman-to-integer/

class Solution:
    def romanToInt(self, s: str) -> int:
        count=0
        for i, j in enumerate(s):
            if j == "I":
                count+=1
            if j == "V":
                count+=5
            if j == "X":
                count+=10
            if j == "L":
                count+=50
            if j == "C":
                count+=100
            if j == "D":
                count+=500
            if j == "M":
                count+=1000

        if "IV" in s:
            count-=2
        if "IX" in s:
            count-=2
        if "XL" in s:
            count-=20
        if "XC" in s:
            count-=20
        if "CD" in s:
            count-=200
        if "CM" in s:
            count-=200

        return count