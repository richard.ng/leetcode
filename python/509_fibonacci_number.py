# https://leetcode.com/problems/fibonacci-number/

class Solution:
    def fib(self, N: int) -> int:
        if N == 0:
            return 0
        if N == 1:
            return 1
        if N > 1:
            A=[1]*2
            for i in range(2,N+1):
                A[0],A[1]=0,1
                A.append(A[i-1]+A[i-2])
            return A[N]