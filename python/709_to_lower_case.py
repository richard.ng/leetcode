# https://leetcode.com/problems/to-lower-case/

class Solution:
    def toLowerCase(self, str: str) -> str:
        A=[]
        for i in range(len(str)):
            A.append(str[i])
            if ord(str[i])>90:
                continue
            elif ord(str[i])<65:
                continue
            else:
                A[i]=chr(ord(str[i])+32)
        return "".join(A)