#  https://leetcode.com/problems/reverse-integer/

class Solution:
    def reverse(self, x: int) -> int:

        if x == 0:
            return 0

        y = abs(x)

        rev = []

        while y % 10 == 0:
            y = y/10

        string=str(int(y))

        ans = string[::-1]

        if not -2**31 < int(ans) < (2**31)-1:
            return 0
        if x < 0:
            return("-"+ans)
        return ans
