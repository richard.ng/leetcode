// https://leetcode.com/problems/palindrome-number/

import (
    "strconv"
)

func isPalindrome(x int) bool {
    y  := strconv.Itoa(x)
    
    for i, _ := range y {
        
		if y[i] !=  y[len(y) - 1 - i] {
			return false
	    }
        
    }
    
	return true
}