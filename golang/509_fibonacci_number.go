// https://leetcode.com/problems/fibonacci-number/

func fib(N int) int {
    T1:=0
	T2:=1
	a:=make([]int,32)
	a[0]=T1
	a[1]=T2
    
	for i:=2;i<len(a);i++{
	a[i]=a[i-1]+a[i-2]
	}
    
    return a[N]
}