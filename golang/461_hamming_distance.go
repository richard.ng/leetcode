// https://leetcode.com/problems/hamming-distance/

func hammingDistance(x int, y int) int {
	z:=x^y
    count:=0
	for i:=0;z!=0;i++ {
		z &= z-1
        count++
	}
    return count
}