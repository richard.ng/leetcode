// https://leetcode.com/problems/to-lower-case/

func toLowerCase(str string) string {
	k:=[]byte(str)
	for i:=0;i<len(k);i++{
		if k[i] >= 65 && k[i] <= 90 {
			k[i] = k[i] + 32
		}
	}
    return string(k)
}