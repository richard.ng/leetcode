// https://leetcode.com/problems/palindrome-number/

import (
    "strconv"
)

func isPalindrome(x int) bool {
    if x < 0 {
        return false
    }
    
    if x % 10 == 0 && x>0 {
        return false
    }
    
    y  := strconv.Itoa(x)
    
    for i:=0; i<len(y)/2;i++ {
        
		if y[i] !=  y[len(y) - 1 - i] {
			return false
	    }
        
    }

	return true
}