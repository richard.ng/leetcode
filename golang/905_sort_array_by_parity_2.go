// https://leetcode.com/problems/sort-array-by-parity/

func sortArrayByParity(A []int) []int {
    i := 0
	j := len(A) - 1
    
    for i < j {
        if (A[i] % 2 > A[j] % 2) {
            tmp := A[i];
            A[i] = A[j];
            A[j] = tmp;
            }

        if A[i] % 2 == 0 {
            i++
		}
        if A[j] % 2 == 1 {
		    j--
		}
		
        }
    return A
}