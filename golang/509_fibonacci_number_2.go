// https://leetcode.com/problems/fibonacci-number/

func fib(N int) int {
	a := make([]int, 31)

	for i := 0; i < len(a); i++ {
		if i == 0 {
			a[i] = 0
		}
		if i == 1 {
			a[i] = 1
		}
		if i > 1 {
			a[i] = a[i-1] + a[i-2]
		}
	}

	return(a[N])
}